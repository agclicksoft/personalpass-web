const PROXY_CONFIG = [{
  context : ['/api'],
  target : 'https://personal-pass-api.herokuapp.com',
  secure : true,
  logLevel : 'debug',
  changeOrigin: true
}]

module.exports = PROXY_CONFIG
