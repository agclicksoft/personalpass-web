import { TestBed } from '@angular/core/testing';

import { ProfessionalsService } from './professionals.service';

describe('ProfessionalsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfessionalsService = TestBed.get(ProfessionalsService);
    expect(service).toBeTruthy();
  });
});
