import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Professionals } from '../models/professionals';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfessionalsService {

  constructor(
    private http: HttpClient
  ) { }

  getAllProfessionals(page) {
    return this.http.get<Professionals>(environment.API + 'professionals?page=' + page)
  }

  getProfessionals(id) {
    return this.http.get<Professionals>(environment.API + 'professionals/' + id)
  }

  putProfessionals(data, id) {
    return this.http.put<Professionals>(environment.API + 'professionals/' + id, data)
  }

  deleteProfessionals(id) {
    return this.http.delete<Professionals>(`${environment.API}professionals/${id}`)
  }

  findProfessionals(data) {
    return this.http.get<Professionals>(environment.API + 'professionals?name=' + data)
  }
  postAttachments(userID, body) {
    return this.http.post<any>(`${environment.API}users/${userID}/attachments`, body)
  }
  deleteAttachments(attachments) {
    return this.http.request('delete', `${environment.API}users/attachments`, { body: {attachments: attachments} });
  }
}
