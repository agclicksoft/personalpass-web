import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Services } from '../models/services';

@Injectable({
  providedIn: 'root'
})
export class ScheduledServicesService {

  constructor(
    private http: HttpClient
  ) { }

  // REQUISIÇÕES HTTP RELACIONADAS AOS SERVIÇOS
  getAllScheduledServices(page) {
    return this.http.get<Services>(environment.API + 'scheduled-services?page=' + page)
  }

  getScheduledServices(id) {
    return this.http.get<Services>(environment.API + 'scheduled-services/' + id)
  }

  putStatus(data, id) {
    return this.http.put<any>(environment.API + 'scheduled-services/' + id + '/status', data)
  }

  getAllScheduledServicesByAProfessional(id, page) {
    return this.http.get<Services>(`${environment.API}professionals/${id}/schedule?page=${page}`);
  }

  findScheduledServices(data) {
    return this.http.get<Services>(environment.API + 'scheduled-services?name=' + data);
  }

  findAllScheduledServicesByAProfessional(id, page, name) {
    return this.http.get<Services>(`${environment.API}professionals/${id}/schedule?page=${page}&name=${name}`);
  }

}
