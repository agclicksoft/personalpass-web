import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { States, Districts, Neighborhoods } from '../models/localizations';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {

  constructor(
    private http: HttpClient
  ) { }

  localizationsAPI: string = 'https://servicodados.ibge.gov.br/api/v1/localidades/'

  getStates() {
    return this.http.get<Array<States>>(this.localizationsAPI + 'estados');
  }

  getDistricts(statesId) {
    return this.http.get<Array<Districts>>(this.localizationsAPI + 'estados/' + statesId + '/municipios');
  }

  getNeighborhoods(districtsId) {
    return this.http.get<Array<Neighborhoods>>(this.localizationsAPI + 'municipios/' + districtsId + '/subdistritos');
  }

}
