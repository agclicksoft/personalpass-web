import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from 'src/environments/environment';

import { Students } from '../models/students';


@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  constructor(
    private http: HttpClient
  ) { }

  // REQUISIÇÕES HTTP RELACIONADAS AOS ALUNOS
  getAllStudents(page) {
    return this.http.get<Students>(environment.API + 'students?page=' + page)
  }

  getStudents(id) {
    return this.http.get<Students>(environment.API + 'students/' + id)
  }

  putStudents(data, id) {
    return this.http.put<Students>(environment.API + 'students/' + id, data)
  }

  deleteStudents(id) {
    return this.http.delete<Students>(`${environment.API}students/${id}`)
  }

  findStudents(data) {
    return this.http.get<Students>(environment.API + 'students?name=' + data)
  }

  postAttachments(userID, body) {
    return this.http.post<any>(`${environment.API}users/${userID}/attachments`, body)
  }
  
  deleteAttachments(attachments) {
    return this.http.request('delete', `${environment.API}users/attachments`, { body: {attachments: attachments} });
  }
}
