// LOCALIZAÇÕES VINDAS DA API. MÚLTIPLOS ɵBROWSER_SANITIZATION_PROVIDERS, CIDADES E ESTADOS
export class Localizations{
    states: Array<States> = [new States()];
    districts: Array<Districts> = [new Districts()];
    neighborhoods: Array<Neighborhoods> = [new Neighborhoods()];
    cep: Object;
}

// LOCALIZAÇÕES SELECIONADAS PELO USUÁRIO. ÚNICO ESTADO, BAIRRO E DIVERSOS BAIRROS
export class Localization{
    state: any = new States();
    district: any = new Districts();
    neighborhood: Array<any> = [];
    otherNeighborhoods: Array<any> = [];
}

export class States {
    id: number;
    nome: string;
}

export class Districts {
    id: number;
    nome: string;
}

export class Neighborhoods {
    id: number;
    nome: string;
}