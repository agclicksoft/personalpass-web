export class Establishments {
    id: number;
    name: string; 
    cnpj: string;
    address_zipcode: string;
    address_street: string; 
    address_number: string; 
    address_complement: string;
    address_city: string;
    address_uf: string;
    address_neighborhood: string; 
    created_at: Date; 
    updated_at: Date;
}