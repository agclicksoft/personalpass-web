import { Professionals} from './professionals';
import { Establishments } from './establishments';
import { Students } from './students';

// CLASSE UTILIZADA APENAS DA PAGINAÇÃO
class ServicesData {
    professional: Professionals = new Professionals();
    establishments: Establishments = new Establishments();
    students: Students = new Students();
    id: number;
    professional_id: number;
    establishment_id: number;
    status: string;
    date: Date;
    description: string;
    service: string;
    total: number;
    elapsed_time: Date;
    created_at: Date;
    updated_at: Date;
}

export class Services {
    // PROPRIEDADES ORIGINAIS DE SERVIÇOS
    professional: Professionals = new Professionals();
    establishments: Establishments = new Establishments();
    students: Students = new Students();
    id: number;
    professional_id: number;
    establishment_id: number;
    status: string;
    date: Date;
    description: string;
    service: string;
    elapsed_time: Date;
    created_at: Date;
    updated_at: Date;                                     

    // PROPRIEDADE USADA PARA QT TOTAL DE SERVIÇOS E VALOR TOTAL DO SERVIÇO
    total: number;

    // PROPRIEDADES USADAS APENAS NA PAGINAÇÃO
    perPage: number;
    page: number;
    lastPage: number;
    data: Array<ServicesData> = [new ServicesData()];
}