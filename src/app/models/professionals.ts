import { Users } from './users';
import { Services } from './services';

export class Professionals {
    // PROPRIEDADES ORIGINAIS DE PROFISSIONAIS
    user: Users = new Users;
    id: string;
    user_id: string;
    doc_identity: string;
    contact: string;
    bank_name: string;
    bank_type: string;
    bank_number: string;
    bank_agency: string;
    bibliography: string;
    emergency_phone: string;
    professionalServiceLocations: Array<any> = [new Localization()];
    scheduledServices: Array<any> = [];
    specialties: Array<any> = [];
    doc_professional: string;
    created_at: Date;
    updated_at: Date;

    // PROPRIEDADES USADAS APENAS NA PAGINAÇÃO
    total: number;
    perPage: number;
    page: number;
    lastPage: number;
    data: Array<ProfessionalsData> = [new ProfessionalsData];
}

// CLASSE UTILIZADA APENAS NA PAGINAÇÃO
class ProfessionalsData {
    user: Users = new Users;
    id: string;
    user_id: string;
    doc_identity: string;
    contact: string;
    bank_name: string;
    bank_type: string;
    bank_number: string;
    bank_agency: string;
    bibliography: string;
    emergency_phone: string;
    professionalServiceLocations: Array<Localization> = [new Localization()];
    scheduledServices: Array<Services> = [];
    doc_professional: string;
    created_at: Date;
    updated_at: Date;
}

class Localization {
    state: string;
    state_id: number;
    district: string;
    district_id: number;
    neighborhood: Array<any> = [];
}
