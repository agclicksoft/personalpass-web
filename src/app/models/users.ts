import { Attachments } from './attachments';

export class Users {
    id: number;
    username: string;
    email: string;
    remember: boolean;
    password: string;
    cpf: string;
    contact: string;
    emergency_phone: string;
    genre: string;
    birthday: Date;
    name: string;
    role: string;
    img_profile: string;
    address_zipcode: string;
    address_street: string;
    address_number: string;
    address_complement: string;
    address_city: string;
    address_uf: string;
    address_neighborhood: string;
    created_at: Date;
    updated_at: Date;
    attachments: Array<Attachments>;
    faqAnswered: Array<FaqAnswered> = [];

}

class FaqAnswered {
    faq_question_id: number;
    user_id: number;
    answered: string;
}
