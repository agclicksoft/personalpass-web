export class Attachments {
  id: number;
  name: string;
  path: string;
  key: string;
}
