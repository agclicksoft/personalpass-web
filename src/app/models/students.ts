import { Users } from './users';

export class Students {

    // PROPRIEDADES ORIGINAIS DE PROFISSIONAIS
    id: number;
    user_id: number;
    created_at: Date;
    updated_at: Date;;
    user: Users = new Users();
    classes: [];


    // PROPRIEDADES USADAS APENAS NA PAGINAÇÃO
    total: number;
    perPage: number;
    page: number;
    lastPage: number;
    data: Array<StudentsData> = [new StudentsData()];
}

class StudentsData {
    id: number;
    user_id: number;
    created_at: Date;
    updated_at: Date;;
    user: Users = new Users();
    classes: []
}
