import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxPaginationModule } from "ngx-pagination";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxMaskModule } from "ngx-mask";
import { NgxPhoneMaskBrModule } from "ngx-phone-mask-br"
import { AutosizeModule } from 'ngx-autosize';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    NgxPhoneMaskBrModule,
    AutosizeModule,
    NgSelectModule,
    NgxMaskModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: "toast-bottom-right",
      preventDuplicates: true
    })
  ],
  exports: [
    BrowserAnimationsModule,
    NgxPaginationModule, 
    NgxSpinnerModule,
    NgxPhoneMaskBrModule,
    AutosizeModule,
    NgSelectModule,
    NgxMaskModule,
    ToastrModule
  ]
})
export class UtilitiesModule {}
