import { Component, OnInit } from '@angular/core';

import { Users } from '../../models/users';

import { AuthService } from '../../auth/auth.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: Users = new Users;

  // FORM VALIDATORS, JUST SUBMIT IF NO ERRORS (EMPTY FIELDS)
  error = { email: false, password: false }

  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  }

  onSubmit() {

    if (this.user.email) {
      // IF user.email ISN'T EMPTY, RETURN FALSE TO ERROR
      this.error.email = false;
    } else {
      // IF user.email IS EMPTY, RETURN TRUE TO ERROR
      this.error.email = true;
    }

    if (this.user.password) {
      // IF user.password ISN'T EMPTY, RETURN FALSE TO ERROR
      this.error.password = false;
    } else {
      // IF user.password IS EMPTY, RETURN TRUE TO ERROR
      this.error.password = true;
    }

    // JUST SUBMIT FORM IF HAVE NO ERRORS
    if (this.error.email === false && this.error.password === false) {
      
      this.spinner.show();

      return this.authService.authenticate(this.user)
    }
  }

}
