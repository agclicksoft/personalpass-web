import { Component, OnInit } from '@angular/core';

import { Students } from 'src/app/models/students';
import { StudentsService } from 'src/app/api/students.service';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';



@Component({
  selector: 'app-listagem-de-alunos',
  templateUrl: './listagem-de-alunos.component.html',
  styleUrls: ['./listagem-de-alunos.component.css']
})
export class ListagemDeAlunosComponent implements OnInit {

  students: Students = new Students()

  p: number = 1;

  constructor(
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private studentsService: StudentsService
  ) { }

  ngOnInit() {
    this.getAllStudents();
  }

  // BUSCA TODOS OS ALUNOS DO SISTEMA
  getAllStudents() {
    this.spinner.show()
    this.studentsService.getAllStudents(this.p).subscribe(
      res => { this.students = res; },
      err => { this.toastr.error(err.error[0].message); this.spinner.hide(); },
      ()  => { this.spinner.hide(); }
    )
  }

  // BUSCA ALUNOS COM OS DADOS DIGITADOS NO CAMPO DE BUSCA
  findStudents(data) {
    this.studentsService.findStudents(data).subscribe(
      res => { this.students = res },
      err => { this.toastr.error() }
    )
  }

  // FUNÇÃO PARA EFETUAR PAGINAÇÃO NO BACKEND
  pageChanged(page) {
    this.spinner.show()
    this.p = page
    this.studentsService.getAllStudents(this.p).subscribe(
      res => { this.students = res; },
      err => { this.spinner.hide(), this.toastr.error(err.error[0].message) },
      ()  => { this.spinner.hide(); }
    )
  }

}
