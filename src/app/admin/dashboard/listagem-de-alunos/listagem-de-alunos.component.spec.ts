import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDeAlunosComponent } from './listagem-de-alunos.component';

describe('ListagemDeAlunosComponent', () => {
  let component: ListagemDeAlunosComponent;
  let fixture: ComponentFixture<ListagemDeAlunosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDeAlunosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDeAlunosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
