import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosDoAlunoComponent } from './dados-do-aluno.component';

describe('DadosDoAlunoComponent', () => {
  let component: DadosDoAlunoComponent;
  let fixture: ComponentFixture<DadosDoAlunoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosDoAlunoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosDoAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
