import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

import { Students } from 'src/app/models/students';
import { StudentsService } from 'src/app/api/students.service';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-dados-do-aluno',
  templateUrl: './dados-do-aluno.component.html',
  styleUrls: ['./dados-do-aluno.component.css']
})
export class DadosDoAlunoComponent implements OnInit {

  students: Students = new Students();

  id: number;

  readOnly: boolean;

  constructor(
    private studentsService: StudentsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.parent.params.id
  }

  ngOnInit() {
    this.getStudents();
    this.readMode();
  }

  // BUSCA OS DADOS DO ALUNO
  getStudents() {
    this.spinner.show();
    this.studentsService.getStudents(this.id)
    .subscribe(
      res => { this.students = res },
      err => { this.spinner.hide(), this.toastr.error(err.error[0].message) },
      ()  => { this.spinner.hide() }
    )
  }

  // FORMULÁRIO EM MODO DE EDIÇÃO
  editMode(){
    this.readOnly = false;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.removeAttribute('readonly'));
  }

  // FORMULÁRIO EM MODO DE LEITURA
  readMode() {
    this.readOnly = true;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.setAttribute('readonly', 'true'));
    this.getStudents();
  }

  // ATUALIZAR OS DADOS DO ALUNO
  onSubmit() {
    this.spinner.show();
    this.studentsService.putStudents(this.students, this.id)
    .subscribe(
      res => { this.students = res, this.toastr.success('Dados atualizados.') },
      err => { this.toastr.error(err.error) },
      ()  => { this.spinner.hide(), this.readMode() }
    )
  }

  deleteAluno() {
    if (confirm('Deseja excluir todos as informações deste aluno? Esta ação é irreversível.'))
      this.studentsService.deleteStudents(this.id).subscribe(
        res => { this.toastr.success('Aluno excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/alunos']) }
      )
  }
}
