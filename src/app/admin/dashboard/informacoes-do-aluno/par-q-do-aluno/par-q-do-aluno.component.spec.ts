import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParQDoAlunoComponent } from './par-q-do-aluno.component';

describe('ParQDoAlunoComponent', () => {
  let component: ParQDoAlunoComponent;
  let fixture: ComponentFixture<ParQDoAlunoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParQDoAlunoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParQDoAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
