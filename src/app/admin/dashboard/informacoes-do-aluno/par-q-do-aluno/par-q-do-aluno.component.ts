import { Component, OnInit } from '@angular/core';
import { Students } from 'src/app/models/students';
import { StudentsService } from 'src/app/api/students.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-par-q-do-aluno',
  templateUrl: './par-q-do-aluno.component.html',
  styleUrls: ['./par-q-do-aluno.component.css']
})
export class ParQDoAlunoComponent implements OnInit {

  students: Students = new Students();

  readOnly: boolean;

  faq_answered: Array<any> = [];

  id: number;

  constructor(
    private studentsService: StudentsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.parent.params.id;
  }

  ngOnInit() {
    this.readMode();
    this.getStudents();
    this.setFaqAndUserId();
  }

  getStudents() {
    this.spinner.show();
    this.studentsService.getStudents(this.id).subscribe(
      res => { 
        this.students = res,
        this.setAnswereds(),
        this.spinner.hide()
      },
      err => {
        this.toastr.error(),
        this.spinner.hide()
      }
    )
  }

  // MONTA O ARRAY DE OBJETOS COM ID DA QUESTÃO, ID DO USUÁRIO E A RESPOSTA PADRÃO COMO 'NULL'
  setFaqAndUserId() {
    if (this.students.user.faqAnswered.length === 0) 
      for (let i = 1; i <= 21; i++)
        this.faq_answered.push({
          faq_question_id: i,
          user_id: this.id,
          answered: null
         })
  }

  // SE O USUÁRIO TIVER ALGUMA PERGUNTA RESPONDIDA, ADICIONA ESSAS PERGUNTAS AO 'faq_answered'
  // PARA FAZER O TWO WAY DATA BINDING
  setAnswereds() {
    if (this.students.user.faqAnswered.length > 0) {
      for (let i in this.students.user.faqAnswered)
        this.faq_answered[i].answered = this.students.user.faqAnswered[i].answered

      if (this.students.user.faqAnswered[3].answered.indexOf('1') > -1)
        this.faq_answered[3].tontura = true;

      if (this.students.user.faqAnswered[3].answered.indexOf('2') > -1)
        this.faq_answered[3].enjoo = true;

      if (this.students.user.faqAnswered[3].answered.indexOf('3') > -1)
        this.faq_answered[3].falta_de_ar = true;

      if (this.students.user.faqAnswered[3].answered.indexOf('4') > -1)
        this.faq_answered[3].nenhum = true;
    }
  }

  onSubmit() {
    this.spinner.show();
    
    let checkboxAnswered = '';

    if (this.faq_answered[3].tontura) {
      checkboxAnswered += '1';
      delete this.faq_answered[3].tontura
    }

    if (this.faq_answered[3].enjoo) {
      checkboxAnswered += '2';
      delete this.faq_answered[3].enjoo
    }

    if (this.faq_answered[3].falta_de_ar) {
      checkboxAnswered += '3';
      delete this.faq_answered[3].falta_de_ar
    }

    if (this.faq_answered[3].nenhum) {
      checkboxAnswered = '4';
      delete this.faq_answered[3].nenhum
    }

    checkboxAnswered = checkboxAnswered.split('').join(',');

    this.faq_answered[3].answered = checkboxAnswered;

    this.students.user.faqAnswered = this.faq_answered;    

    this.studentsService.putStudents(this.students, this.id).subscribe(
      res => { 
        this.students = res,
        this.setAnswereds(),
        this.readMode();
        this.toastr.success('Dados atualizados');
        this.spinner.hide();
      },
      err => { this.toastr.error('Erro ao atualizar dados') }
    )
  }

  editMode(){
    this.readOnly = false;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.removeAttribute('disabled'));
  }

  readMode() {
    this.readOnly = true;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.setAttribute('disabled', 'true'));
  }

  deleteAluno() {
    if (confirm('Deseja excluir todos as informações deste aluno? Esta ação é irreversível.'))
      this.studentsService.deleteStudents(this.id).subscribe(
        res => { this.toastr.success('Aluno excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/alunos']) }
      )
  }
  

}
