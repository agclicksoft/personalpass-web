import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnexosDoAlunoComponent } from './anexos-do-aluno.component';

describe('AnexosDoAlunoComponent', () => {
  let component: AnexosDoAlunoComponent;
  let fixture: ComponentFixture<AnexosDoAlunoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnexosDoAlunoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnexosDoAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
