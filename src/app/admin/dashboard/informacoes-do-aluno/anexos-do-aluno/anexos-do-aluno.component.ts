
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Students } from 'src/app/models/students';
import { StudentsService } from 'src/app/api/students.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-anexos-do-aluno',
  templateUrl: './anexos-do-aluno.component.html',
  styleUrls: ['./anexos-do-aluno.component.css']
})
export class AnexosDoAlunoComponent implements OnInit {

  students: Students = new Students();

  id: number;

  readOnly: boolean;

  anexos: any;

  uploadForm: FormGroup;

  formData = new FormData();

  AttachmentsSelect: Array<number>;

  @ViewChild("fileUpload", {static: false}) fileUpload: ElementRef;files  = [];

  constructor(
    private studentsService: StudentsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.parent.params.id
  }

  ngOnInit() {
    this.getStudents();
    this.readMode();
    this.uploadForm = this.formBuilder.group({
      attachments: ['']
    });
  }

  // BUSCA OS DADOS DO ALUNO
  getStudents() {
    this.spinner.show();
    this.studentsService.getStudents(this.id).subscribe(
      res => { this.students = res, this.spinner.hide() },
      err => { this.spinner.hide(), this.toastr.error(err.error[0].message) },
    )
  }

  // FORMULÁRIO EM MODO DE EDIÇÃO
  editMode(){
    this.readOnly = false;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.removeAttribute('readonly'));
  }

  // FORMULÁRIO EM MODO DE LEITURA
  readMode() {
    this.readOnly = true;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.setAttribute('readonly', 'true'));
    this.getStudents();
  }

  enviarAnexo(){
    this.spinner.show();
    this.studentsService.postAttachments(this.students.user_id, this.formData).subscribe(
      res => { 
        this.getStudents(),
        this.toastr.success('Anexo(s) enviado(s) com sucesso'),
        document.getElementById('close').click()
        this.spinner.hide();
      },
      err => {
        this.toastr.error(err.error[0].message),
        this.spinner.hide();
      }
    )
  }

  onFileSelect(event) {
    const fomtAux = new FormData();
    this.formData = fomtAux;
    if (event.target.files.length > 0) {
      // tslint:disable-next-line: prefer-for-of
      for (let index = 0; index < event.target.files.length; index++) {
        const element = event.target.files[index];
        this.formData.append('attachments', element);
      }
    }
  }

  openAttachment(url) {
    window.open(url, '_blank');
  }

  deletarAnexo(f) {

    this.AttachmentsSelect = [];

    for (let property in f.value)
      if (f.value[property] === true)
        this.AttachmentsSelect.push(parseInt(property));

    if (confirm('Confirme a exclusão do anexo')) 
      this.studentsService.deleteAttachments(this.AttachmentsSelect).subscribe(
        res => { this.toastr.success('Anexo excluido com sucesso') },
        err => { this.toastr.error('Error ao excluir anexo') },
        ()  => { this.readMode() }
      );
  }

  deleteAluno() {
    if (confirm('Deseja excluir todos as informações deste aluno? Esta ação é irreversível.'))
      this.studentsService.deleteStudents(this.id).subscribe(
        res => { this.toastr.success('Aluno excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/alunos']) }
      )
  }
}
