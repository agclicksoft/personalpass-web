import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacoesDoAlunoComponent } from './informacoes-do-aluno.component';

describe('InformacoesDoAlunoComponent', () => {
  let component: InformacoesDoAlunoComponent;
  let fixture: ComponentFixture<InformacoesDoAlunoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacoesDoAlunoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacoesDoAlunoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
