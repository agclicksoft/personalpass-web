import { Component, OnInit } from "@angular/core";

import { Professionals } from "src/app/models/professionals";
import { ProfessionalsService } from 'src/app/api/professionals.service';

import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-listagem-de-profissionais",
  templateUrl: "./listagem-de-profissionais.component.html",
  styleUrls: ["./listagem-de-profissionais.component.css"]
})

export class ListagemDeProfissionaisComponent implements OnInit {

  professionals: Professionals = new Professionals();

  p: number = 1;

  constructor(
    private professionalService: ProfessionalsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private professionalsService: ProfessionalsService
  ) {}

  ngOnInit() {
    this.getAllProfessionals();
  }

  // BUSCA TODOS OS PROFISSIONAIS DO SISTEMA
  getAllProfessionals() {
    this.spinner.show();
    this.professionalService.getAllProfessionals(this.p).subscribe(
      res => { this.professionals = res; },
      err => { this.toastr.error(err.error[0].message); this.spinner.hide(); },
      ()  => { this.spinner.hide() }
    );
  }

  // BUSCA PROFISSIONAIS COM OS DADOS DIGITADOS NO CAMPO DE BUSCA
  findProfessionals(data) {
    this.professionalsService.findProfessionals(data).subscribe(
      res => { this.professionals = res },
      err => { this.toastr.error() }
    )
  }

  // EFETUA A PAGINAÇÃO NO BACKEND
  pageChanged(page) {
    this.spinner.show();
    this.p = page;
    this.professionalService.getAllProfessionals(this.p).subscribe(
      res => { this.professionals = res; },
      err => { this.toastr.error(err.error), this.spinner.hide(); },
      ()  => { this.spinner.hide(); }
    );
  }
}


