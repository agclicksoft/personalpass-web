import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDeProfissionaisComponent } from './listagem-de-profissionais.component';

describe('ListagemDeProfissionaisComponent', () => {
  let component: ListagemDeProfissionaisComponent;
  let fixture: ComponentFixture<ListagemDeProfissionaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDeProfissionaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDeProfissionaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
