import { AuthService } from '../../auth/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  toggleControl: number = 0;

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authService.logout('admin');
  }

  menuToggle() {
    if (window.innerWidth < 768) {

      if(this.toggleControl === 0) {
        document.querySelector('nav').setAttribute('style', 'top: 0;');
        document.querySelectorAll('.menu-bar').forEach( el => el.setAttribute('style', 'background: #FFF;'));
        this.toggleControl = 1;
      } 
      
      else {
        document.querySelector('nav').setAttribute('style', 'top: -100vh');
        document.querySelectorAll('.menu-bar').forEach( el => el.removeAttribute('style'))
        this.toggleControl = 0;
      }

    } 
  }
}
