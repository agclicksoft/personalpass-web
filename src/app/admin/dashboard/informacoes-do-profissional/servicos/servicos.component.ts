import { Component, OnInit } from '@angular/core';
import { ScheduledServicesService } from 'src/app/api/scheduled-services.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Services } from 'src/app/models/services';
import { ActivatedRoute } from '@angular/router';
import { Professionals } from '../../../../models/professionals';
import { ProfessionalsService } from 'src/app/api/professionals.service';

@Component({
  selector: 'app-servicos',
  templateUrl: './servicos.component.html',
  styleUrls: ['./servicos.component.css']
})
export class ServicosComponent implements OnInit {

  scheduledServices: Services = new Services();

  professionals: Professionals = new Professionals();

  p: number = 1;

  id: number;

  constructor(
    private scheduledServicesService: ScheduledServicesService,
    private professionalsService: ProfessionalsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute
  ) { 
    this.id = this.activatedRoute.snapshot.parent.params.id;
  }

  ngOnInit() {
    this.getProfessionals();
  }

  onSubmit() {
    
  }

  getProfessionals() {
    this.spinner.show();
    this.professionalsService.getProfessionals(this.id)
    .subscribe(
      res => { this.professionals = res, this.spinner.hide() },
      err => { this.toastr.error() }  
    )
  }


  findAllScheduledServicesByAProfessional(data) {
    this.scheduledServicesService.findAllScheduledServicesByAProfessional(this.id, this.p, data).subscribe(
      res => { this.professionals.scheduledServices = res.data },
      err => { this.toastr.error(err.error) }
    )
  }

}
