import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Professionals } from '../../../../models/professionals';
import { ProfessionalsService } from 'src/app/api/professionals.service';

import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { StudentsService } from 'src/app/api/students.service';

@Component({
  selector: 'app-dados',
  templateUrl: './dados.component.html',
  styleUrls: ['./dados.component.css']
})
export class DadosComponent implements OnInit {

  professionals: Professionals = new Professionals();

  id: number;

  readOnly: boolean;

  constructor(
    private professionalServices: ProfessionalsService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.id = this.activatedRoute.snapshot.parent.params.id
  }

  ngOnInit() {
    this.readMode();
    this.getProfessionals();
  }

  // BUSCA O RROFISSIONAL ESPECÍFICO PELO ID
  getProfessionals() {
    this.spinner.show();
    this.professionalServices.getProfessionals(this.id).subscribe(
      res => { this.professionals = res },
      err => { this.toastr.error(err.error[0].message), this.spinner.hide() },
      ()  => { this.spinner.hide(), this.defaultLocalizations(); }
    )
  }

  // DEFINE professionalServiceLocations E neighborhoods COMO UM ARRAY NÃO VAZIA PARA EVITAR ERROS AO SALVAR
  defaultLocalizations() {
    this.professionals.professionalServiceLocations = new Professionals().professionalServiceLocations;
  }

  // ENVIO DO FORMULÁRIO PARA EDITAR INFORMAÇÕES DO PROFISSIONAL
  onSubmit() {
    this.spinner.show();
    this.professionalServices.putProfessionals(this.professionals, this.id).subscribe(
      res => { this.professionals = res, this.toastr.success('Dados atualizados.') },
      err => { this.toastr.error('Não foi possível atualizar os dados.'), this.spinner.hide() },
      ()  => { this.spinner.hide(), this.readMode(), this.defaultLocalizations() }
    )    
  }

  // MODO DE EDIÇÃO DO FORMULÁRIO
  editMode() {
    this.readOnly = false;
    const input = document.querySelectorAll('input, textarea');
    input.forEach(el => el.removeAttribute('readonly'));
  }

  // MODO DE LEITURA DO FORMULÁRIO
  readMode() {
    this.readOnly = true;
    this.getProfessionals();
    const input = document.querySelectorAll('input, textarea');
    input.forEach(el => el.setAttribute('readonly', 'true'));
  }

  deleteProfessional() {
    if (confirm('Deseja excluir todos as informações deste profissional? Esta ação é irreversível.'))
      this.professionalServices.deleteProfessionals(this.id).subscribe(
        res => { this.toastr.success('Profissional excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/profissionais']) }
      )
  }

}
