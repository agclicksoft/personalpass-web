import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Professionals } from 'src/app/models/professionals';
import { Localizations, Localization } from 'src/app/models/localizations';
import { ProfessionalsService } from 'src/app/api/professionals.service';
import { LocalizationService } from 'src/app/api/localization.service';


import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormArray } from '@angular/forms';


@Component({
  selector: 'app-locais-de-atendimento',
  templateUrl: './locais-de-atendimento.component.html',
  styleUrls: ['./locais-de-atendimento.component.css']
})
export class LocaisDeAtendimentoComponent implements OnInit {

  // DADOS DO PROFESSINAL A SER CARREGADO E ATUALIZADO
  professional: Professionals = new Professionals();

  // ARRAY COM TODOS OS ESTADOS, TODAS AS CIDADES DE UM ESTADO E TODOS OS BAIRROS DE UMA CIDADE
  localizations: Localizations = new Localizations;

  // OBJETO A SER ARMAZENADO OS VALORES DO ESTADO, CIDADE E BAIRRO ACIMA SELECIONADO
  localization: Localization = new Localization;

  // ARRAY PARA ARMAZENAR APENAS OS NOMES DE CADA BAIRRO, FORA DE QUALQUER OBJETO OU SEM QUALQUER OUTRA PROPRIEDADE
  neighborhoods: Array<string> = [];

  // ID DO PROFESSIONAL ATUAL
  id: number;

  // VARIÁVEL PARA CONTROLAR MODO DE LEITURA, OU MODO DE EDIÇÃO.
  readOnly: boolean;

  stateHasChanged: boolean = false;

  constructor(
    private professionalService: ProfessionalsService,
    private localizationService: LocalizationService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.parent.params.id;
  }

  localizationForm = this.fb.group({
    otherNeighborhoods: this.fb.array([
      
    ])
  })

  get otherNeighborhoods() {
    return this.localizationForm.get('otherNeighborhoods') as FormArray;
  }

  addOtherNeighborhoods() {
    this.otherNeighborhoods.push(this.fb.control(''));
  }

  ngOnInit() {
    this.readMode();
    this.getProfessionals();
  }

  onSubmit() {
    // this.spinner.show();

    // REMOVE QUALQUER BAIRRO ARMAZENADO NO ARRAY DE BAIRROS A SER SALVO NOS DADOS DO PROFISSIONAL
    this.neighborhoods = [];
    
    // ARMAZENA NA VARIÁVEL 'neighborhoods' APENAS OS NOMES DOS BAIRROS SELECIONADOS
    for (let i in this.localization.neighborhood)
      this.neighborhoods.push(this.localization.neighborhood[i].nome);

    for (let i in this.localizationForm.value.otherNeighborhoods)
      if (this.localizationForm.value.otherNeighborhoods[i] !== '')
        this.neighborhoods.push(this.localizationForm.value.otherNeighborhoods[i]);

    if (this.neighborhoods[0] == undefined) {
      this.neighborhoods = [];
      
      for (let i in this.professional.professionalServiceLocations)
        this.neighborhoods.push(this.professional.professionalServiceLocations[i].neighborhood);

      for (let i in this.localizationForm.value.otherNeighborhoods)
        if (this.localizationForm.value.otherNeighborhoods[i] !== '')
          this.neighborhoods.push(this.localizationForm.value.otherNeighborhoods[i]);
    }
    

    if (!this.localization.state.nome) {
      this.localization.state.nome = this.professional.professionalServiceLocations[0].state;
      this.localization.state.id = this.professional.professionalServiceLocations[0].state_id;
    }

    if (!this.localization.district.nome) {
      this.localization.district.nome = this.professional.professionalServiceLocations[0].district;
      this.localization.district.id = this.professional.professionalServiceLocations[0].district_id;
    }

    // REMOVE QUALQUER DADO JÁ ARMAZENADO NOS LOCAIS DE ATENDIMENTO DO PROFISSIONAL
    this.professional.professionalServiceLocations = [];

    // ADICIONA AOS LOCAIS DE ATENDIMENTO DO PROFESSIONAL O ESTADO, CIDADE E BAIRRO (APENAS OS NOMES)
    this.professional.professionalServiceLocations.push( {
      state: this.localization.state.nome,
      state_id: this.localization.state.id,
      district: this.localization.district.nome,
      district_id: this.localization.district.id,
      neighborhood: this.neighborhoods
    } );

    // ENVIA A REQUISIÇÃO JÁ COM OS DADOS FORMATADOS, APÓS RECEBER UMA RESPOSTA DE SUCESSO ALTERNA PARA MODO DE LEITURA.
    this.professionalService.putProfessionals(this.professional, this.id)
    .subscribe(
      res => ( this.professional = res, this.readMode(), this.toastr.success('Dados atualizados') ),
      err => { this.spinner.hide(), this.toastr.error(err.error.message) },
      ()  => { this.spinner.hide() }
    )
  }

  // BUSCA UM PROFISSIONAL PELO ID
  getProfessionals() {
    this.spinner.show();
    this.professionalService.getProfessionals(this.id).subscribe(
      res => { this.professional = res, this.spinner.hide() },
      err => { this.spinner.hide(), this.toastr.error(err.error) },
    )
  }


  // BUSCA TODOS OS ESTADOS DO BRASIL E TRAZ A RESPOSTA ORDENADA POR ORDEM ALFABÉTICA
  getStates() {
    this.localization.neighborhood = [];
    this.localizationService.getStates().subscribe(
      res => { this.localizations.states = res.sort( (a, b) => a.nome.localeCompare(b.nome)) },
      err => { this.toastr.error(err.error) },
    )
  }

  // BUSCA TODAS AS CIDADES DO ESTADO ACIMA SELECIONADO, BASEADO NO SEU ID
  getDistricts() {
    this.stateHasChanged = true;
    this.localization.neighborhood = [];
    this.localizationService.getDistricts(this.localization.state.id).subscribe(
      res => { this.localizations.districts = res.sort( (a, b) => a.nome.localeCompare(b.nome)) },
      err => { this.toastr.error(err.error) }
    )
  }

  // BUSCA TODOS OS BAIRROS DA CIDADE ACIMA SELECIONADA, BASEADO NO SEU ID
  getNeighborhoods() {
    this.localization.neighborhood = [];
    this.localizationService.getNeighborhoods(this.localization.district.id).subscribe(
      res => { this.localizations.neighborhoods = res.sort( (a, b) => a.nome.localeCompare(b.nome)) },
      err => { this.toastr.error(err.error) }
    )
  }

  setLocalizations() {
    if (this.professional.professionalServiceLocations.length > 0){
      this.localizationService.getDistricts(this.professional.professionalServiceLocations[0].state_id).subscribe(
        res => { this.localizations.districts = res.sort( (a, b) => a.nome.localeCompare(b.nome)) },
        err => { this.toastr.error(err.error) }
      );

      this.localizationService.getNeighborhoods(this.professional.professionalServiceLocations[0].district_id).subscribe(
        res => { this.localizations.neighborhoods = res.sort( (a, b) => a.nome.localeCompare(b.nome)) },
        err => { this.toastr.error(err.error) }
      )

      for (let i in this.professional.professionalServiceLocations)
        this.localization.neighborhood.push(this.professional.professionalServiceLocations[i].neighborhood);
    }
  }

  // ALTERNA PARA O MODO DE EDIÇÃO
  editMode(){
    this.readOnly = false;
    this.getStates();
    this.setLocalizations();
  }

  // ALTERNA PARA O MODO DE LEITURA
  readMode() {
    this.readOnly = true;
    this.stateHasChanged = false;
    this.localization = new Localization();
  }

  deleteProfessional() {
    if (confirm('Deseja excluir todos as informações deste profissional? Esta ação é irreversível.'))
      this.professionalService.deleteProfessionals(this.id).subscribe(
        res => { this.toastr.success('Profissional excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/profissionais']) }
      )
  }

}
