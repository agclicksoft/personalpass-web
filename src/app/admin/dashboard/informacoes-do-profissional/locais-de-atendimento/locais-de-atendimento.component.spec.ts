import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocaisDeAtendimentoComponent } from './locais-de-atendimento.component';

describe('LocaisDeAtendimentoComponent', () => {
  let component: LocaisDeAtendimentoComponent;
  let fixture: ComponentFixture<LocaisDeAtendimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocaisDeAtendimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocaisDeAtendimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
