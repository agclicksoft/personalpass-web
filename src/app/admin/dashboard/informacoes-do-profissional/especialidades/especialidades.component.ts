import { Component, OnInit } from '@angular/core';
import { Professionals } from 'src/app/models/professionals';
import { ProfessionalsService } from 'src/app/api/professionals.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-especialidades',
  templateUrl: './especialidades.component.html',
  styleUrls: ['./especialidades.component.css']
})
export class EspecialidadesComponent implements OnInit {

  professional: Professionals = new Professionals();

  readOnly: boolean;

  checkControl: any = [];

  id: number;

  specialties: Array<any> = [
    { description: "Hipertrofia muscular"},
    { description: "Emagrecimento"},
    { description: "Definição muscular"},
    { description: "Condicionamento físico"},
    { description: "Musculação"},
    { description: "Pilates"},
    { description: "Mais idade"},
    { description: "Outros"},
  ];

  constructor(
    private professionalsServices: ProfessionalsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { 
    this.id = this.activatedRoute.snapshot.parent.params.id;
  }

  ngOnInit() {
    this.getProfessionals();
    this.readMode();
  }

  // BUSCA O PROFESSIONAL ESPECÍFICO PELO ID, E SELECIONA O CHECKBOX COM AS HABILIDADES DO PROFISSIONAL
  getProfessionals() {
    this.spinner.show();
    this.professionalsServices.getProfessionals(this.id).subscribe(
      res => { this.professional = res, this.checkSpecialties() },
      err => { this.spinner.hide(), this.toastr.error() },
      ()  => { this.spinner.hide(), this.defaultLocalizations(); }
    )
  }

  defaultLocalizations() {
    this.professional.professionalServiceLocations = new Professionals().professionalServiceLocations;
  }

  // PARA CADA ESPECIALDADE CADASTRADA NO PROFISSIONAL, DÁ UM PUSH AO ARRAY 'checkControl'
  // E LOGO APÓS TRANSFORMA ESSE ARRAY EM UMA STRING COM TODAS AS ESPECIALIDADES.
  // CASO O 'description' DE ALGUMA FUNCIONALIDADE ESTEJA CONTIDU NA STRING 'checkControl'
  // ADICIONA A PROPRIEDADE 'checked: true' AO ARRAY 'specialtie' O QUAL FAZ OS CHECKBOXS VIREM MARCADOS
  // AO CARREGAR A PÁGINA.
  checkSpecialties() {
    this.checkControl = [];

    for (let i in this.professional.specialties)
      this.checkControl.push(this.professional.specialties[i].description);

    this.checkControl = this.checkControl.join(', ');

    for (let i in this.specialties) 
      if (this.checkControl.indexOf(this.specialties[i].description) !== -1 )
        this.specialties[i].checked = true;
  }

  // AO ENVIAR O FORMULÁRIO, APAGA TODOS OS ITENS DO ARRAY DE ESPECIALIDADES DO PROFISSIONAL
  // E ADICIONA APENAS AS QUE ESTIVEREM MARCADAS COMO TRUE NA PÁGINA. APÓS, EXCLUI A PROPRIEDADE CHECKED E
  // ADICIONA A PROPRIEDADE 'professional_id' COM O ID DO PROFISSIONAL.
  onSubmit() {
    this.spinner.show();

    this.professional.specialties = [];

    for(let i in this.specialties)
      if( this.specialties[i].checked){
        delete this.specialties[i].checked
        this.specialties[i].professional_id = this.id;
        this.professional.specialties.push(this.specialties[i])
      }
            
    this.professionalsServices.putProfessionals(this.professional, this.id)
    .subscribe(
      res => { this.toastr.success('Dados atualizados.'), this.checkSpecialties(); },
      err => { this.spinner.hide(), this.toastr.error() },
      ()  => { this.spinner.hide(), this.readMode(); }
    )
  }

  // HABILITA O USUÁRIO EDITAR O FORMULÁRIO
  editMode(){
    this.readOnly = false;
    var input = document.querySelectorAll('input');
    input.forEach(el => el.removeAttribute('disabled'));
  }

  // DESABILITA O USUÁRIO EDITAR O FORMULÁRIO
  readMode() {
    this.readOnly = true;
    var input = document.querySelectorAll('input');
    input.forEach(el => el.setAttribute('disabled', 'true'));
  }

  deleteProfessional() {
    if (confirm('Deseja excluir todos as informações deste profissional? Esta ação é irreversível.'))
      this.professionalsServices.deleteProfessionals(this.id).subscribe(
        res => { this.toastr.success('Profissional excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/profissionais']) }
      )
  }

}
