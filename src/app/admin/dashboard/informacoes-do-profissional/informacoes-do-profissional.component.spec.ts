import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacoesDoProfissionalComponent } from './informacoes-do-profissional.component';

describe('InformacoesDoProfissionalComponent', () => {
  let component: InformacoesDoProfissionalComponent;
  let fixture: ComponentFixture<InformacoesDoProfissionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacoesDoProfissionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacoesDoProfissionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
