import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProfessionalsService } from 'src/app/api/professionals.service';
import { Professionals } from 'src/app/models/professionals';

@Component({
  selector: 'app-anexos',
  templateUrl: './anexos.component.html',
  styleUrls: ['./anexos.component.css']
})
export class AnexosComponent implements OnInit {

  professionals: Professionals = new Professionals();

  id: number;

  readOnly: boolean;

  anexos: any;

  uploadForm: FormGroup;

  formData = new FormData();

  AttachmentsSelect: Array<number>;

  constructor(
    private professionalsService: ProfessionalsService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.id = this.activatedRoute.snapshot.parent.params.id
  }

  ngOnInit() {
    this.readMode();
    this.uploadForm = this.formBuilder.group({
      attachments: ['']
    });
  }

  getProfessionals() {
    this.spinner.show();
    this.professionalsService.getProfessionals(this.id).subscribe(
      res => { this.professionals = res, this.spinner.hide() },
      err => { this.spinner.hide(), this.toastr.error(err.error[0].message) }
    )
  }


  // FORMULÁRIO EM MODO DE EDIÇÃO
  editMode(){
    this.readOnly = false;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.removeAttribute('readonly'));
  }

  // FORMULÁRIO EM MODO DE LEITURA
  readMode() {
    this.readOnly = true;
    const input = document.querySelectorAll('input');
    input.forEach(el => el.setAttribute('readonly', 'true'));
    this.getProfessionals();
  }

  enviarAnexo(){
    this.professionalsService.postAttachments(this.professionals.user_id, this.formData).subscribe(
      res => { 
        this.getProfessionals(),
        this.toastr.success('Anexo(s) enviado(s) com sucesso'),
        document.getElementById('close').click()
      },
      err => { this.toastr.error('Erro ao enviar anexo') }
    )
  }

  onFileSelect(event) {
    const fomtAux = new FormData();
    this.formData = fomtAux;
    if (event.target.files.length > 0) {
      // tslint:disable-next-line: prefer-for-of
      for (let index = 0; index < event.target.files.length; index++) {
        const element = event.target.files[index];
        this.formData.append('attachments', element);
      }
    }
  }

  openAttachment(url) {
    window.open(url, '_blank');
  }

  deletarAnexo(f) {

    this.AttachmentsSelect = [];

    for (let property in f.value)
      if (f.value[property] === true)
        this.AttachmentsSelect.push(parseInt(property));

    if (confirm('Confirme a exclusão do anexo')) 
      this.professionalsService.deleteAttachments(this.AttachmentsSelect).subscribe(
        res => { this.getProfessionals(), this.toastr.success('Anexo excluido com sucesso') },
        err => { this.toastr.error('Error ao excluir anexo') },
        ()  => { this.readMode() }
      );
  }

  deleteProfessional() {
    if (confirm('Deseja excluir todos as informações deste profissional? Esta ação é irreversível.'))
      this.professionalsService.deleteProfessionals(this.id).subscribe(
        res => { this.toastr.success('Profissional excluído com sucesso.') },
        err => { this.toastr.error(err.error) },
        ()  => { this.router.navigate(['/admin/dashboard/profissionais']) }
      )
  }

}
