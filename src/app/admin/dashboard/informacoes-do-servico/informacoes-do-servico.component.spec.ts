import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacoesDoServicoComponent } from './informacoes-do-servico.component';

describe('InformacoesDoServicoComponent', () => {
  let component: InformacoesDoServicoComponent;
  let fixture: ComponentFixture<InformacoesDoServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacoesDoServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacoesDoServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
