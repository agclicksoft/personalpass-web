import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Services } from 'src/app/models/services';
import { ScheduledServicesService } from 'src/app/api/scheduled-services.service';

import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-informacoes-do-servico',
  templateUrl: './informacoes-do-servico.component.html',
  styleUrls: ['./informacoes-do-servico.component.css']
})
export class InformacoesDoServicoComponent implements OnInit {

  id: String;

  constructor(
    private scheduledServicesService: ScheduledServicesService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {
    this.id = this.activatedRoute.snapshot.params.id
  }

  service: Services = new Services;

  ngOnInit() {
    this.getScheduledServices();
  }

  // BUSCA UM SERVIÇO ESPECÍFICO PELO ID
  getScheduledServices() {
    this.spinner.show();
    this.scheduledServicesService.getScheduledServices(this.id).subscribe(
      res => { this.service = res },
      err => { this.spinner.hide(), this.toastr.error(err.error[0].message) },
      ()  => { this.spinner.hide() }
    )
  }

  // ATUALIZA APENAS O STATUS DO SERVIÇO
  updateStatus() {
    this.spinner.show();
    const data = {status: this.service.status};
    this.scheduledServicesService.putStatus(data, this.id).subscribe(
      res => { this.service = res, this.toastr.success('Status atualizado.') },
      err => { this.spinner.hide(), this.toastr.error(err.error) },
      ()  => { this.spinner.hide(), document.getElementById('cancelar').click() }
    )
  }

  // ENVIA UMA NOVA REQUISIÇÃO PARA BUSCAR O SERVIÇO E RETONAR AS INFORMAÇÕES ORIGINAIS
  resetStatus() {
    this.getScheduledServices();
  }

}
