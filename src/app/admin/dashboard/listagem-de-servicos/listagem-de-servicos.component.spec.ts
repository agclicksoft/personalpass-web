import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemDeServicosComponent } from './listagem-de-servicos.component';

describe('ListagemDeServicosComponent', () => {
  let component: ListagemDeServicosComponent;
  let fixture: ComponentFixture<ListagemDeServicosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemDeServicosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemDeServicosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
