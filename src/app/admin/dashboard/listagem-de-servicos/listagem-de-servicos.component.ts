import { Component, OnInit } from "@angular/core";

import { Services } from "src/app/models/services";
import { ScheduledServicesService } from 'src/app/api/scheduled-services.service';

import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-listagem-de-servicos",
  templateUrl: "./listagem-de-servicos.component.html",
  styleUrls: ["./listagem-de-servicos.component.css"]
})
export class ListagemDeServicosComponent implements OnInit {
  services: Services = new Services();

  p: number = 1;

  constructor(
    private scheduledServicesService: ScheduledServicesService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getAllScheduledServices();
  }

  // BUSCA TODOS OS SERVIÇOS DO SISTEMA
  getAllScheduledServices() {
    this.spinner.show();
    this.scheduledServicesService.getAllScheduledServices(this.p).subscribe(
      res => { this.services = res },
      err => { this.toastr.error(err.error), this.spinner.hide() },
      ()  => { this.spinner.hide() }
    );
  }

  // EFETUA A PAGINAÇÃO NO BACKEND
  pageChanged(page) {
    this.spinner.show();
    this.p = page;
    this.scheduledServicesService.getAllScheduledServices(this.p).subscribe(
      res => { this.services = res; },
      err => { this.toastr.error(err.error), this.spinner.hide(); },
      ()  => { this.spinner.hide(); }
    );
  }

  // BUSCA OS SERVIÇOS DO SISTEMA COM OS DADOS DIGITADOS NO CAMPO DE BUSCA
  findServices(data) {
    this.scheduledServicesService.findScheduledServices(data).subscribe(
      res => { this.services = res },
      err => { this.toastr.error() }
    )
    
  }
}
