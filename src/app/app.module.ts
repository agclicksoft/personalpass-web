import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { AuthModule } from './auth/auth.module';
import { UtilitiesModule } from './utilities/utilities.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './admin/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { ListagemDeServicosComponent } from './admin/dashboard/listagem-de-servicos/listagem-de-servicos.component';
import { InformacoesDoServicoComponent } from './admin/dashboard/informacoes-do-servico/informacoes-do-servico.component';
import { ListagemDeProfissionaisComponent } from './admin/dashboard/listagem-de-profissionais/listagem-de-profissionais.component';
import { InformacoesDoProfissionalComponent } from './admin/dashboard/informacoes-do-profissional/informacoes-do-profissional.component';
import { ListagemDeAlunosComponent } from './admin/dashboard/listagem-de-alunos/listagem-de-alunos.component';
import { InformacoesDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/informacoes-do-aluno.component';
import { DadosComponent } from './admin/dashboard/informacoes-do-profissional/dados/dados.component';
import { EspecialidadesComponent } from './admin/dashboard/informacoes-do-profissional/especialidades/especialidades.component';
import { LocaisDeAtendimentoComponent } from './admin/dashboard/informacoes-do-profissional/locais-de-atendimento/locais-de-atendimento.component';
import { ServicosComponent } from './admin/dashboard/informacoes-do-profissional/servicos/servicos.component';
import { AnexosComponent } from './admin/dashboard/informacoes-do-profissional/anexos/anexos.component';
import { AnexosDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/anexos-do-aluno/anexos-do-aluno.component';
import { DadosDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/dados-do-aluno/dados-do-aluno.component';
import { ParQDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/par-q-do-aluno/par-q-do-aluno.component';
import { RelatoriosComponent } from './admin/dashboard/relatorios/relatorios.component';

registerLocaleData(localePt, 'pt');

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ListagemDeServicosComponent,
    InformacoesDoServicoComponent,
    ListagemDeProfissionaisComponent,
    InformacoesDoProfissionalComponent,
    ListagemDeAlunosComponent,
    InformacoesDoAlunoComponent,
    DadosComponent,
    EspecialidadesComponent,
    LocaisDeAtendimentoComponent,
    ServicosComponent,
    AnexosComponent,
    AnexosDoAlunoComponent,
    DadosDoAlunoComponent,
    ParQDoAlunoComponent,
    RelatoriosComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    UtilitiesModule,

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
