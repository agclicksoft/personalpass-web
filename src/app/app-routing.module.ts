import { AuthGuard } from './auth/auth.guard';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { LoginComponent } from './admin/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListagemDeAlunosComponent } from './admin/dashboard/listagem-de-alunos/listagem-de-alunos.component';
import { InformacoesDoServicoComponent } from './admin/dashboard/informacoes-do-servico/informacoes-do-servico.component';
import { ListagemDeProfissionaisComponent } from './admin/dashboard/listagem-de-profissionais/listagem-de-profissionais.component';
import { ListagemDeServicosComponent } from './admin/dashboard/listagem-de-servicos/listagem-de-servicos.component';
import { InformacoesDoProfissionalComponent } from './admin/dashboard/informacoes-do-profissional/informacoes-do-profissional.component';
import { DadosComponent } from './admin/dashboard/informacoes-do-profissional/dados/dados.component';
import { EspecialidadesComponent } from './admin/dashboard/informacoes-do-profissional/especialidades/especialidades.component';
import { LocaisDeAtendimentoComponent } from './admin/dashboard/informacoes-do-profissional/locais-de-atendimento/locais-de-atendimento.component';
import { ServicosComponent } from './admin/dashboard/informacoes-do-profissional/servicos/servicos.component';
import { AnexosComponent } from './admin/dashboard/informacoes-do-profissional/anexos/anexos.component';
import { InformacoesDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/informacoes-do-aluno.component';
import { DadosDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/dados-do-aluno/dados-do-aluno.component';
import { ParQDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/par-q-do-aluno/par-q-do-aluno.component';
import { AnexosDoAlunoComponent } from './admin/dashboard/informacoes-do-aluno/anexos-do-aluno/anexos-do-aluno.component';
import { RelatoriosComponent } from './admin/dashboard/relatorios/relatorios.component';

const routes: Routes = [
  { path: '', redirectTo: 'admin/login', pathMatch: 'full' },
  {path: 'admin/login', component: LoginComponent},
  {path: 'admin/dashboard', component: DashboardComponent, canActivate: [AuthGuard], children: [
    {path: '', pathMatch: 'full', redirectTo: 'servicos'},
    {path: 'servicos', component: ListagemDeServicosComponent},
    {path: 'servicos/:id', component: InformacoesDoServicoComponent},
    {path: 'profissionais', component: ListagemDeProfissionaisComponent},
    {path: 'profissionais/:id', component: InformacoesDoProfissionalComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'dados'},
      {path: 'dados', component: DadosComponent},
      {path: 'especialidades', component: EspecialidadesComponent},
      {path: 'locais-de-atendimento', component: LocaisDeAtendimentoComponent},
      {path: 'servicos', component: ServicosComponent},
      {path: 'anexos', component: AnexosComponent}
    ]},
    {path: 'alunos', component: ListagemDeAlunosComponent},
    {path: 'alunos/:id', component: InformacoesDoAlunoComponent, children: [
      {path: '', pathMatch: 'full', redirectTo: 'dados'},
      {path: 'dados', component: DadosDoAlunoComponent },
      {path: 'par-q', component: ParQDoAlunoComponent },
      {path: 'anexos', component: AnexosDoAlunoComponent }
    ]},
    {path: 'relatorios', component: RelatoriosComponent}
  ]
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule {
 }
