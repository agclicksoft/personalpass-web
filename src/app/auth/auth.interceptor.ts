import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import * as moment from 'moment';

import { environment } from 'src/environments/environment';



@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    // Interceptando todas as requisições http e se url da requisição for igual a url API 
    // e se tiver um token em local storage, ele adicionar Bearer + token ao header da requisição

    //INTERCEPTANDO TODAS AS REQUISIÇÕES HTTP
    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {

        const token = localStorage.getItem('token') || sessionStorage.getItem('token');

        // VERIFICANDO SE A REQUISIÇÃO É PARA A API DA APLICAÇÃO E SE O USUÁRIO POSSUI UM TOKEN
        if (req.url.indexOf(environment.API) === 0 && token) {

            // CASO SEJA VERIFICADO, CLONA A REQUISIÇÃO E ADICIONA UM AUTHORIZATION HEADER COM 'Bearer' PRECEDENDO O TOKEN
            const cloned = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + token) });

            // LOGO APÓS, ATUALIZA O TEMPO DE EXPIRAÇÃO DA AUTENTICAÇÃO DO USUÁRIO PARA 15 MINUTOS.
            const expiresAt = moment().add(15, 'minutes');

            sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));

            // EM VEZ DE ENVIAR A REQUISIÇÃO ORIGINAL, ENVIA A REQUISAÇÃO CLONADA COM O BEARER TOKEN NO HEADER DE AUTORIZAÇÃO
            return next.handle(cloned);
        }
        else {
            
            // CASO O ENDEREÇO DA REQUISIÇÃO NÃO SEJA DA NOSSA API, OU USUÁRIO NÃO POSSUA TOKEN, É ENVIADO A REQUISIÇÃO ORIGINAL,
            // SEM O BEARER TOKEN NO HEADER DE AUTORIZAÇÃO
            return next.handle(req);
        }
    }
}
