import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { Users } from '../models/users';

import { Md5 } from 'ts-md5/dist/md5';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  // ENVIANDO OS DADOS DE LOGIN PARA A API. CASO O USUÁRIO SEJA AUTENTICADO, INICIA UMA SESSÃO COM A RESPOSTA RECEBIDA DA API
  authenticate(user) {

    this.http.post<Users>(environment.API + 'users/authenticate', user)
      .subscribe(
        res => {
          if (user.remember === true)
            return this.setLocal(res)
          else
            return this.setSession(res) 
        },
        err => { this.spinner.hide(), this.toastr.error() },

        ()  => { this.spinner.hide(), this.router.navigate(['/admin/dashboard']) }
      );
  }

  // ARMAZENA O TOKEN E O TEMPO DE INATIVIDADE (15 MINUTOS) EM SESSION STORAGE.
  private setSession(res) {

    const expiresAt = moment().add(15, 'minutes');

    sessionStorage.setItem('token', res.token);

    sessionStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  // ARMAZENA O TOKEN E O TEMPO DE EXPIRAÇÃO (7 DIAS) EM LOCALSTORAGE
  private setLocal(res) {

    const expiresAt = moment().add(7, 'days');

    localStorage.setItem('token', res.token);

    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  // REMOVE O TOKEN E O TEMPO DE EXPIRAÇÃO DE LOCALSTORAGE E SESSION STORAGE E O TEMPO DE EXPIRAÇÃO E REDIRECIONADO
  // O USUÁRIO PARA O PATH DE LOGIN CORRESPONDENTE AO SEU ROLE (PERFIL)
  logout(path) {

    sessionStorage.clear();

    localStorage.clear();

    this.router.navigate(['/' + path + '/login']);
  }

  // VERIFICA SE O USUÁRIO AINDA ESTÁ VALIDADO NO SISTEMA
  public isLoggedIn() {

    return moment().isBefore(this.getExpiration());

  }

  // RETORNA O MOMENTO EXATO DE EXPIRAÇÃO DO TOKEN
  getExpiration() {

    const expiration = localStorage.getItem('expires_at') || sessionStorage.getItem('expires_at');

    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }
}
